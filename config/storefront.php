<?php
return [
    /**
     * Register here for all the page components in store-front module.
     * Format: 
     * (as a new array element) 
     * 'slug' => 'Component', (remove the .vue).
     */
    'components' => [
        'home' => 'Home',
        'category' => 'Category',
        'terms' => 'Terms',
        'privacy' => 'Privacy',
        'faq' => 'Faq',
        'talent-acquisition' => 'TalentAcquisition',
        'video-tutorial' => 'VideoTutorial',
        'beginner-guide' => 'BeginnerGuide',
        'company-profile' => 'CompanyProfile',
        'shipping-policy' => 'ShippingPolicy',
    ],
];
?>