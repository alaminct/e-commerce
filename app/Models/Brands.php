<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;

class Brands extends Model implements HasMedia
{
    use HasFactory;
    use HasTranslations;
    use HasUploader;
    use InteractsWithMedia;
    use SoftDeletes;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'description'
    ];

    /**
     * Translatable fields
     *
     * @var array
     */
    public $translatable = ['title'];

    /**
     * Register media resolution and size
     * automatically provide images that match the size needed.
     *
     * @param Media $media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('logo')
            ->width(120)
            ->height(80)
            ->performOnCollections('brand-logo')
            ->fit(Manipulations::FIT_CROP, 120, 80)
            ->nonQueued();
    }
}
