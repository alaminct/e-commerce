<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;

class Products extends Model
{
    use HasFactory;
    use HasTranslations;
    use HasUploader;
    use InteractsWithMedia;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'photo', 'name', 'description', 'price', 'user_id', 'reviewed_at', 'reviewed_by', 'slug', 'sku', 'number', 'categorycode', 'licensed', 'bulk', 'tags', 'categories', 'location', 'share', 'share_type', 'status'];

    /**
     * The attributes that will be translatable.
     *
     * @var array
     */
    public $translatable = ['name', 'description',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['user_id'];

    /**
     * Convert image to the desired size.
     *
     * @param Media $media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(400)
            ->performOnCollections('product-thumbnail')
            ->fit(Manipulations::FIT_CROP, 400, 400)
            ->nonQueued();

        $this->addMediaConversion('gallery')
            ->width(400)
            ->height(400)
            ->performOnCollections('product-gallery')
            ->fit(Manipulations::FIT_CROP, 400, 400)
            ->nonQueued();
    }

    public function brand()
    {
        return $this->hasOne(Brands::class, 'id', 'brand_id');
    }

    public function category()
    {
        return $this->hasOne(CategoryProducts::class, 'product_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
