<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//  use Spatie\Translatable\HasTranslations;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;
class VendorUserTem extends Model
{

    use HasFactory;
    // use HasTranslations;
    use HasFactory;
    use HasUploader;
    use InteractsWithMedia;


    protected $guarded = [];


    public function br_file(Media $media = null): void
    {
        $this->addMediaConversion('br_file')
            ->performOnCollections('br_file')
            ->fit(Manipulations::FIT_CROP, 300, 300)
            ->nonQueued();
    }

    public function company_photo(Media $media = null): void
    {
        $this->addMediaConversion('componay_photo')
        ->width(300)
        ->height(300)
        ->performOnCollections('componay_photo',300,300 )
        ->fit(Manipulations::FIT_CROP)
        ->nonQueued();
    }
}
