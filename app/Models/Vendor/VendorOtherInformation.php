<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vendor\Vendor;
use Illuminate\Notifications\Notifiable;
class VendorOtherInformation extends Model
{
    use  Notifiable, HasFactory;

    protected $guarded = [];

    protected $table='vendor_other_information';


    public function routeNotificationForMail($notification)
    {
        // Return email address only...
        return $this->registrant_email;

    }


    public function vandor()
    {
        return $this->hasOne(Vendor::class, 'id', 'vendor_id');
    }

}


