<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;

class ProductCategory extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia; 
    use HasUploader;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    // protected $with = ['locale'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'type', 'parent_id', 'banner', 'featured', 'commission'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'featured' => 'boolean',
    ];

    /**
     * Convert image to the desired size.
     *
     * @param Media $media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('desktop')
            ->width(800)
            ->height(300)
            ->performOnCollections('banner-en-desktop', 'banner-hk-desktop', 'banner-cn-desktop')
            ->fit(Manipulations::FIT_CROP, 800, 300)
            ->nonQueued();

        $this->addMediaConversion('tablet')
            ->width(600)
            ->height(400)
            ->performOnCollections('banner-en-tablet', 'banner-hk-tablet', 'banner-cn-tablet')
            ->fit(Manipulations::FIT_CROP, 600, 400)
            ->nonQueued();

        $this->addMediaConversion('mobile')
            ->width(420)
            ->height(420)
            ->performOnCollections('banner-en-mobile', 'banner-hk-mobile', 'banner-cn-mobile')
            ->fit(Manipulations::FIT_CROP, 420, 420)
            ->nonQueued();
    }

    public function locales()
    {
        return $this->hasMany(CategoryLocale::class, 'category_id');
    }

    public function locale()
    {
        return $this->hasOne(CategoryLocale::class, 'category_id');
    }

    public function children()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasMany(ProductCategory::class, 'id', 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(CategoryProducts::class, 'category_id');
    }
}
