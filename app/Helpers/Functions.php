<?php

use App\Models\ProductCategory as Categories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

if(!function_exists('slugify')){
    /**
     * slugify function
     * slugify any models that has slug column
     *
     * @param [string] $string
     * @param [Collection] $model
     * @return void
     */
    function slugify($string, $model, $needle = 'slug')
    {
        $slugged = Str::slug($string, '-');
        if($slugCount = $model->where($needle, 'like', $slugged)->count() > 0){
            return $slugged.'-'.$slugCount;
        }else{
            return $slugged;
        }
    }

}

if (!function_exists('testExport')) {
    /**
     * Change all instances of :argument to {argument}
     *
     * @param $string
     * @return void
     *
     */
    function testExport($string)
    {
        array_walk_recursive($string, function (&$v, $k) {
            $v = preg_replace('/:(\w+)/', '{$1}', $v);
        });

        return $string;
    }
}

if(!function_exists('parseLanguageList')){
    /**
     * Parse languages available in a browser.
     *
     * @param [type] $languageList
     * @return void
     */
    function parseLanguageList($languageList)
    {
        if (is_null($languageList)) {
            if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                return array();
            }
            $languageList = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }
        $languages = array();
        $languageRanges = explode(',', trim($languageList));
        foreach ($languageRanges as $languageRange) {
            if (preg_match('/(\*|[a-zA-Z0-9]{1,8}(?:-[a-zA-Z0-9]{1,8})*)(?:\s*;\s*q\s*=\s*(0(?:\.\d{0,3})|1(?:\.0{0,3})))?/', trim($languageRange), $match)) {
                if (!isset($match[2])) {
                    $match[2] = '1.0';
                } else {
                    $match[2] = (string) floatval($match[2]);
                }
                if (!isset($languages[$match[2]])) {
                    $languages[$match[2]] = array();
                }
                $languages[$match[2]][] = strtolower($match[1]);
            }
        }
        krsort($languages);
        // dd($languages);
        $return = [];
        foreach($languages as $lang){
            // dump($lang);
            $return[] = $lang[0];
        }
        // dd($default);
        return $return;
	}
}

if (!function_exists('hasRole')){
    /**
     * Check if a user has a certain role
     *
     * @param [type] $role
     * @return boolean
     */
    function hasRole($role)
    {
        return auth()->check() && in_array($role, auth()->user()->roles);
    }
}

if(!function_exists('getCategoryNamesByID')){
    function getCategoryNamesByID($array){
        $return = [];
        $cats = Categories::whereIn('id', $array)->with([
            'locale' => function ($query){
                $query->where('locale', app()->getLocale());
            }
        ])->get();
        
        if($cats->count() > 0){
            foreach($cats as $cat){
                $return[] = $cat->locale->title;
            }
        }

        return $return;
    }
}

if(!function_exists('makeParentCategories')){
    function makeParentCategories( $onlyParents = false )
    {
        if($onlyParents){
            return Categories::whereNull('parent_id')->with([
                    'locale' => function ($query) {
                        $query->where('locale', app()->getLocale());
                    },
                ])
                ->orderBy('code', 'asc')
                ->get();
        }else{
            return Categories::whereNull('parent_id')->with([
                    'locale' => function ($query) {
                        $query->where('locale', app()->getLocale());
                    },
                    'children',
                    'children.locale' => function ($query) {
                        $query->where('locale', app()->getLocale());
                    },
                    'children.children',
                    'children.children.locale' => function ($query) {
                        $query->where('locale', app()->getLocale());
                    },
                    'children.children.children',
                    'children.children.children.locale' => function ($query) {
                        $query->where('locale', app()->getLocale());
                    },
                ])
                ->orderBy('code', 'asc')
                ->get();
        }
    }
}

if(!function_exists('getParentTitle')){
    function getParentTitle($parent_id)
    {
        $parent = Categories::where('id', $parent_id)->with([
            'locale' => function($query){
                $query->where('locale', app()->getLocale());
        }])->first();

        // dump($parent->locale->title);

        return $parent->locale->title;
    }
}

if(!function_exists('countryList')){
    function countryList()
    {
        $json = file_get_contents(base_path('/public/data/countries-alpha3.json'));
        // dd($json);
        return collect(json_decode($json));
    }
}

if(!function_exists('getCountryName')){
    function getCountryName($code = null)
    {
        $json = file_get_contents(base_path('/public/data/countries-alpha3.json'));
        // dd($json);
        $countries = collect(json_decode($json));
        return $countries->first(function($value, $key) use($code) {
            return $value->code === $code;
        })->name;
    }
}

if(!function_exists('getCountryISO')){
    function getCountryISO($code = null)
    {
        $json = file_get_contents(base_path('/public/data/countries-alpha3.json'));
        // dd($json);
        $countries = collect(json_decode($json));
        $country = $countries->first(function($value, $key) use($code) {
            return $value->code = $code;
        });
        // dd($country->{'country-code'});

        return str_pad($country->{'country-code'}, 4, '0', STR_PAD_LEFT);
    }
}

if(!function_exists('CSVToArray')){
    function CSVToArray($file)
    {
        $all_rows =[];
        $header = fgetcsv($file);
        while ($row = fgetcsv($file)) {
            $all_rows[] = array_combine($header, $row);
        }

        return $all_rows;
    }
}
?>