<?php

namespace App\Actions\Fortify;

use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        // dd($input);
        
        Validator::make($input, [
            'name' => ['required', 'alpha_spaces', 'latin_only', 'max:255'],
            'title' => ['required'],
            'nickname' => ['required', 'alpha_dash', 'max:100', 'unique:users', 'latin_only'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'mobile' => ['required', 'string', 'max:20'],
            'referer' => ['numeric', 'nullable'],
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => ucwords($input['name']),
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'title' => $input['title'],
                'nickname' => strtolower($input['nickname']),
                'mobile' => $input['mobile'],
                'referer' => $input['referer'],
                'promotion' => $input['promotion'],
                'username' => $input['nickname'],
                'roles' => json_encode(['member']),
            ]), function (User $user) {
                $this->createTeam($user);
            });
        });
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->ownedTeams()->save(Team::forceCreate([
            'user_id' => $user->id,
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            // 'name' => "User's Team",
            'personal_team' => true,
        ]));
    }
}
