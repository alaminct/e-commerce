<?php

namespace App\Actions\Fortify;

use Laravel\Fortify\Rules\Password;

trait PasswordValidationRules
{
    /**
     * Get the validation rules used to validate passwords.
     *
     * @return array
     */
    protected function passwordRules()
    {
        // return ['required', 'string', new Password, 'confirmed']; ## in case the client later requires password to be confirmed.
        
        return ['required', 'string', (new Password)->length(6)->requireNumeric()];
    }
}
