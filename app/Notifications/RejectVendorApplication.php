<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Vendor\VendorOtherInformation;

class RejectVendorApplication extends Notification
{
    use Queueable;

    private $vendororherinformation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(VendorOtherInformation $vendororherinformation )
    {
        $this->vendororherinformation = $vendororherinformation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->line('Dear user'.$this->vendororherinformation->registrant_name)
                ->line('You account has been Rejceted')
                // ->action('Click here  more More Detils ', route('home'))
                ->line('Thank you For your  interest !')
                ->line('admin');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
