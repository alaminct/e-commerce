<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Actions\Fortify\PasswordValidationRules;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class RegisterController extends Controller implements CreatesNewUsers
{
    use PasswordValidationRules;

    public function open($type = 'member')
    {
        $component = null;
        $title = null;
        $page = null;


        switch ($type) {
            case 'member':
                $component = 'Auth/MemberSignUp';
                $title = __('Member Signup');
                $page = 'member-signup';
                break;
            case 'vendor':
                $role = json_encode( auth()->user()->roles ?? '' );
                $role1 =str_replace(".$role.",'vendor','vendor');
                if(isset($role) &&  hasRole($role1)  ){
                    return redirect('vip-membership-plan');
                    }else {
                        $component = 'Auth/VendorSignUp';
                        $title = __('Vendor Signup');
                        $page = 'vendor-signup';
                        break;
                }

            case 'corporate':
                $component = 'Auth/CorporateSignUp';
                $title = __('Corporate Signup');
                $page = 'corporate-signup';
                break;

            default:
                $component = 'Auth/MemberSignUp';
                $title = __('Member Signup');
                $page = 'member-signup';
                break;
        }

        $string = file_get_contents(base_path('CountryPhoneCodes.json'));
        $tcenlish = file_get_contents(base_path('/public/docs/tc-en.html'));
        $tcchinesesimplified = file_get_contents(base_path('/public/docs/tc-zh-CN.html'));
        $tcchinesetraditional = file_get_contents(base_path('/public/docs/tc-zh-HK.html'));
        $privacypolenglish = file_get_contents(base_path('/public/docs/privacypolicy-en.html'));
        $privacypolsimplified = file_get_contents(base_path('/public/docs/privacypolicy-zh-CN.html'));
        $privacypoltraditional = file_get_contents(base_path('/public/docs/privacypolicy-zh-HK.html'));
        // dd($string);
        $arr = json_decode($string, true);
        // dd($arr);
        $CountryPhoneCodes = $arr['countries'];

        return Inertia::render($component, [
            'meta' => [
                'title' => $title
            ],
            'page' => $page,
            'countrycodes' => $CountryPhoneCodes,
            'termsconditions_english' => $tcenlish,
            'termsconditions_chinese_simplified' => $tcchinesesimplified,
            'termsconditions_chinese_traditional' => $tcchinesetraditional,
            'privacy_policy_english' => $privacypolenglish,
            'privacy_policy_simplified' => $privacypolsimplified,
            'privacy_policy_traditional' => $privacypoltraditional,
            'countries' => countryList(),
        ])
        ->withViewData(['title' => $title]);
    }

    public function post(Request $request)
    {
        // dd($request->toArray());
        $user = $this->create($request->toArray());
        // dd($user);
        $user->sendEmailVerificationNotification();
        return redirect()->route('dashboard.index', ['user' => $user->nickname]);
    }

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'alpha_spaces', 'latin_only', 'max:255'],
            'title' => ['required'],
            'nickname' => ['required', 'alpha_dash', 'max:100', 'unique:users', 'latin_only'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'mobile' => ['required', 'string', 'max:20', 'min:11'],
            'referer' => ['numeric', 'nullable'],
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => ucwords($input['name']),
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'title' => $input['title'],
                'nickname' => strtolower($input['nickname']),
                'mobile' => $input['mobile'],
                'referer' => $input['referer'],
                'promotion' => $input['promotion'],
                'username' => strtolower($input['nickname']),
                'locale' => app()->getLocale(),
                'roles' => ['member'],
            ]), function (User $user) {
                $this->createTeam($user);
            });
        });
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->ownedTeams()->save(Team::forceCreate([
            'user_id' => $user->id,
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'personal_team' => true,
        ]));
    }
}
