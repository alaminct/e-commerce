<?php

namespace App\Http\Controllers\Auth;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class LoginController extends Controller
{
    public function member(Request $request)
    {
        return Inertia::render('Auth/MemberLogin', [
            'meta' => [
                'title' => __('Member Login')
            ],
            'page' => 'member-login',
        ])
        ->withViewData(['title' => __('Member Login')]);
    }

    public function vendor(Request $request)
    {
        return Inertia::render('Auth/VendorLogin', [
            'meta' => [
                'title' => __('Vendor Login')
            ],
            'page' => 'vendor-login',
        ])
        ->withViewData(['title' => __('Vendor Login')]);
    }

    public function corporate(Request $request)
    {
        return Inertia::render('Auth/CorporateLogin', [
            'meta' => [
                'title' => __('Corporate Login')
            ],
            'page' => 'corporate-login',
        ])
        ->withViewData(['title' => __('Corporate Login')]);
    }

    public function login(Request $request, $type = null)
    {
        if(null === $type){
            return redirect()->back();
        }
        $remember = $request->has('remember') && $request->remember === 'on';
        Validator::make($request->all(), [
            // 'email' => 'required|email',
              'email' => 'required_without_all:mobile|string',
              'mobile' => 'required_without_all:email,email|numeric|digits:11',
              ])->validate();
            // $user = User::where('mobile', $request->mobile)->orWhere('email', $request->email)->first();

            $email =$request->email;
            $user = User::where('mobile', $request->mobile??$email)->orWhere('email', $request->email??$email)->first();

          if( $user && Auth::attempt(['email' => $user->email, 'password' => $request->password], $remember)){
            // if(Auth::attempt(['email' => $request->email, 'password' => $request->password], true)){

                if (!hasRole($type)) {
                    if (hasRole('admin')) {
                        Auth::logout();

                        $request->session()->invalidate();

                        $request->session()->regenerateToken();
                        return Inertia::location(route('admin.login'));
                    }

                    if (hasRole('dev-admin')) {
                        Auth::logout();

                        $request->session()->invalidate();

                        $request->session()->regenerateToken();
                        return Inertia::location(route('admin.login'));
                    }

                    Auth::logout();

                    $request->session()->invalidate();

                    $request->session()->regenerateToken();

                    return back()->withErrors([
                        'invalid' => __('You do not have access to this member type.'),
                    ]);
                }
                // dd(auth()->user());
                return redirect()->route('dashboard.index', ['user' => Auth::user()->nickname]);
            }else{
                return back()->withErrors([
                    'invalid' => __('Username or password is invalid.'),
                ]);
            }
    }

    public function admin()
    {
        return view('auth.admin-login');
    }

    public function adminpost(Request $request)
    {
        // dd($request);
        $remember = $request->has('remember') && $request->remember === 'on';
        // dd($remember);
        Validator::make($request->all(), [
            'email' => 'required|email',
        ])->validate();

        // dd(Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember));
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)){
            // dd(hasRole('admin'));
            if(!hasRole('admin')){
                if(!hasRole('dev-admin')){
                    Auth::logout();

                    $request->session()->invalidate();

                    $request->session()->regenerateToken();

                    return back()->withErrors([
                        'failed' => __('auth.failed')
                    ]);
                }
            }

            return redirect()->route('admin.index');
        }

        return back()->withErrors([
            'failed' => __('auth.failed')
        ]);
    }
}
