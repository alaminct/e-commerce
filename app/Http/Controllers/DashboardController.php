<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index(Request $request, User $user)
    {
        // dd($user);
        return Inertia::render('Auth/Dashboard', [
            'meta' => [
                'title' => __('User Dashboard')
            ],
            'page' => 'dashboard',
        ])
        ->withViewData(['title' => __('User Dashboard')]);
    }
    public function settings()
    {
        return redirect()->intended(route('storefront.home'));
    }

    public function logout(Request $request)
    {
        ### this is a quick fix to resolve issue with redirection.
        
        if(hasRole('corporate')){
            $type = 'corporate';
        }
        if(hasRole('vendor')){
            $type = 'vendor';
        }
        if(hasRole('member')){
            $type = 'member';
        }

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login.'.$type);
    }
}
