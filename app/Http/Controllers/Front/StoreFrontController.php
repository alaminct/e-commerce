<?php

namespace App\Http\Controllers\Front;

use Product;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class StoreFrontController extends Controller
{
    protected $locales = ['en' => 'en', 'zh-HK' => 'hk', 'zh-CN' => 'cn'];

    public function __construct()
    {
        // 
    }


    public function index(Request $request)
    {
        $prefix = 'Store/';
        $page = config('storefront.components')['home'];
        // dd($page);
        $component = $prefix.$page;
        $featured = $this->dummyProducts('FEATURED', 8);
        $limited = $this->dummyProducts('LIMITED', 8);
        $winter = $this->dummyProducts('WINTER', 8);
        $daily = $this->dummyProducts('DAILY', 8);
        $epidemic = $this->dummyProducts('EPIDEMIC', 8);
        // dd($featured);
        // dd(__('meta.pages.home'));
        return Inertia::render($component, [
            'meta' => [
                'title' => __('meta.pages.home')
            ],
            'products' => [
                'featured' => $featured,
                'limited' => $limited,
                'winter' => $winter,
                'daily' => $daily,
                'epidemic' => $epidemic,
            ],
        ])
        ->withViewData(['title' => __('meta.pages.home')]);
    }
    public function terms(Request $request)
    {
        $tcenlish = file_get_contents(base_path('/public/docs/tc-en.html'));
        $tcchinesesimplified = file_get_contents(base_path('/public/docs/tc-zh-CN.html'));
        $tcchinesetraditional = file_get_contents(base_path('/public/docs/tc-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['terms'];
        
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Terms'
            ],
            'termsconditions_english' => $tcenlish,
            'termsconditions_chinese_simplified' => $tcchinesesimplified,
            'termsconditions_chinese_traditional' => $tcchinesetraditional,
            
        ])
        ->withViewData(['title' => 'Terms']);

    }
    public function privacy(Request $request)
    {
        $privacypolenglish = file_get_contents(base_path('/public/docs/privacypolicy-en.html'));
        $privacypolsimplified = file_get_contents(base_path('/public/docs/privacypolicy-zh-CN.html'));
        $privacypoltraditional = file_get_contents(base_path('/public/docs/privacypolicy-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['privacy'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Privacy'
            ],
            'privacy_policy_english' => $privacypolenglish,
            'privacy_policy_simplified' => $privacypolsimplified,
            'privacy_policy_traditional' => $privacypoltraditional,
        ])
        ->withViewData(['title' => 'Privacy']);

    }

    public function faq(Request $request)
    {
        $faqenglish = file_get_contents(base_path('/public/docs/faq-en.html'));
        $faqsimplified = file_get_contents(base_path('/public/docs/faq-zh-CN.html'));
        $faqtraditional = file_get_contents(base_path('/public/docs/faq-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['faq'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Faq'
            ],
            'faq_english' => $faqenglish,
            'faq_simplified' => $faqsimplified,
            'faq_traditional' => $faqtraditional,
        ])
        ->withViewData(['title' => 'Faq']);

    }

    public function talent_acquisition(Request $request)
    {
        $talentacquenglish = file_get_contents(base_path('/public/docs/talentacquisition-en.html'));
        $talentacqusimplified = file_get_contents(base_path('/public/docs/talentacquisition-zh-CN.html'));
        $talentacqutraditional = file_get_contents(base_path('/public/docs/talentacquisition-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['talent-acquisition'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Talent Acquisition'
            ],
            'talent_acquisition_english' => $talentacquenglish,
            'talent_acquisition_simplified' => $talentacqusimplified,
            'talent_acquisition_traditional' => $talentacqutraditional,
        ])
        ->withViewData(['title' => 'Talent Acquisition']);
    }

    public function beginner_guide(Request $request)
    {
        $beginnerguideenglish = file_get_contents(base_path('/public/docs/beginnerguide-en.html'));
        $beginnerguidesimplified = file_get_contents(base_path('/public/docs/beginnerguide-zh-CN.html'));
        $beginnerguidetraditional = file_get_contents(base_path('/public/docs/beginnerguide-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['beginner-guide'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Beginner Guide'
            ],
            'beginner_guide_english' => $beginnerguideenglish,
            'beginner_guide_simplified' => $beginnerguidesimplified,
            'beginner_guide_traditional' => $beginnerguidetraditional,
        ])
        ->withViewData(['title' => 'Talent Acquisition']);
    }

    public function company_profile(Request $request)
    {
        $companyproenglish = file_get_contents(base_path('/public/docs/companyprofile-en.html'));
        $companyprosimplified = file_get_contents(base_path('/public/docs/companyprofile-zh-CN.html'));
        $companyprotraditional = file_get_contents(base_path('/public/docs/companyprofile-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['company-profile'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Company Profile'
            ],
            'company_profile_english' => $companyproenglish,
            'company_profile_simplified' => $companyprosimplified,
            'company_profile_traditional' => $companyprotraditional,
        ])
        ->withViewData(['title' => 'Company Profile']);
    }

    public function video_tutorial(Request $request)
    {
        $videotuenglish = file_get_contents(base_path('/public/docs/videotutorial-en.html'));
        $videotusimplified = file_get_contents(base_path('/public/docs/videotutorial-zh-CN.html'));
        $videotutraditional = file_get_contents(base_path('/public/docs/videotutorial-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['video-tutorial'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Video Tutorial'
            ],
            'video_tutorial_english' => $videotuenglish,
            'video_tutorial_simplified' => $videotusimplified,
            'video_tutorial_traditional' => $videotutraditional,
        ])
        ->withViewData(['title' => 'Video Tutorial']);
    }

    public function shipping_policy(Request $request)
    {
        $shippingpolicyenglish = file_get_contents(base_path('/public/docs/shippingpolicy-en.html'));
        $shippingpolicysimplified = file_get_contents(base_path('/public/docs/shippingpolicy-zh-CN.html'));
        $shippingpolicytraditional = file_get_contents(base_path('/public/docs/shippingpolicy-zh-HK.html'));
        $prefix = 'Store/';
        $page = config('storefront.components')['shipping-policy'];
        // dd($page);
        $component = $prefix.$page;
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Shipping Policy'
            ],
            'shipping_policy_english' => $shippingpolicyenglish,
            'shipping_policy_simplified' => $shippingpolicysimplified,
            'shipping_policy_traditional' => $shippingpolicytraditional,
        ])
        ->withViewData(['title' => 'Shipping Policy']);
    }

    public function search(Request $request, $cat)
    {
        $prefix = 'Store/';
        $page = config('storefront.components')['category'];
        $locale = app()->getLocale();
        $category = ProductCategory::where('type', $cat)->with([
            'locale' => function ($query) use ($locale) {
                $query->where('locale', $locale);
            }
        ])->first();
        // dd($category);
        $products = null;
        // dd($page);
        $component = $prefix . $page;
        return Inertia::render($component, [
            'meta' => [
                'title' => __('meta.pages.category') . ' | '.$category->locale->title,
            ],
            'products' => $products,
            'page' => $cat,
        ])
        ->withViewData(['title' => __('meta.pages.category')]);
    }

    public function category(Request $request, $type = null)
    {
        $prefix = 'Store/';
        $page = config('storefront.components')['category'];
        $category = ProductCategory::where('type', $type)
        ->with([
            'locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            'children.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            // 'children.children',
            'children.children.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            // 'children.children.children',
            'children.children.children.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            'parent.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            'parent.parent.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
            'parent.parent.parent.locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            },
        ])->first();
        // dd($category);

        $products = Product::fetchByCategories($category->type)->paginate(40);
        $banners = [
            'desktop' => $category->getFirstMediaUrl('banner-'.$this->locales[app()->getLocale()].'-desktop', 'desktop') ?: 'https://dummyimage.com/800x300/FCE7EC/000',
            'tablet' => $category->getFirstMediaUrl('banner-' . $this->locales[app()->getLocale()] . '-tablet', 'tablet') ?: 'https://dummyimage.com/600x400/FCE7EC/000',
            'mobile' => $category->getFirstMediaUrl('banner-' . $this->locales[app()->getLocale()] . '-mobile', 'mobile') ?: 'https://dummyimage.com/420x420/FCE7EC/000',
        ];
        // dd($banners);
        if($products->count() < 1 && (config('app.env') === 'local' || config('app.env') === 'development')){
            $products = $this->dummyProducts($category->locale->title, 65, 40, '//dummyimage.com/175/fafafa/000');
        }
        // dd($products);
        $component = $prefix . $page;
        return Inertia::render($component, [
            'meta' => [
                'title' => __('meta.pages.category') . ' | ' . $category->locale->title,
            ],
            'category' => $category,
            'products' => $products,
            'page' => $type,
            'banners' => $banners,
        ])
        ->withViewData(['title' => __('meta.pages.category')]);
    }

    public function page(Request $request, $slug = null)
    {
        if(null === $slug){
            return redirect('/');
        }

        $prefix = 'Store/';
        $pages = config('storefront.components');
        // dd(array_key_exists($slug, $pages));
        if(array_key_exists($slug, $pages)){
            $component = $prefix . $pages[$slug];
        }else{
            $component = $prefix . 'UnderConstruction';
        }
        
        // dd($featured);
        // dd(__('meta.pages.home'));
        return Inertia::render($component, [
            'meta' => [
                'title' => 'Under Construction'
            ],
            'page' => $slug,
        ])
            ->withViewData(['title' => 'Under Construction']);
    }

    private function dummyProducts($name, $n, $paginate = null, $image = '//dummyimage.com/185/fafafa/000')
    {
        $products = [];
        if(!$paginate){
            for ($i = 0; $i < $n; $i++) {
                $products[$i] = [
                    'img' => $image,
                    'url' => '#',
                    'name' => $name . ' DEMO #' . $i,
                    'details' => [
                        'brand' => 'BRAND X',
                        'size' => '25G'
                    ],
                    'sold' => '2000+',
                    'rating' => [
                        'avg' => 4.50,
                        'valid' => 25,
                    ],
                    'price' => [
                        'normal' => 698.85,
                        'sale' => 398.65,
                    ],
                ];
            }
        }else{
            for ($i = 0; $i < $paginate; $i++) {
                $items[$i] = [
                    'img' => $image,
                    'url' => '#',
                    'name' => $name . ' DEMO #' . $i,
                    'details' => [
                        'brand' => 'BRAND X',
                        'size' => '25G'
                    ],
                    'sold' => '2000+',
                    'rating' => [
                        'avg' => 4.50,
                        'valid' => 25,
                    ],
                    'price' => [
                        'normal' => 698.85,
                        'sale' => 398.65,
                    ],
                ];
            }
            $products['data'] = collect($items);
            // dd(url()->current());
            for($x = 0; $x < ceil($n/$paginate); $x++){
                $links[$x] = [
                    'url' => url()->current(),
                    'label' => $x+1,
                    'active' => false,
                ];
            }
            $links = collect($links);
            $links->prepend([
                'url' => url()->current(),
                'label' => 'Previous',
                'active' => false,
            ])
            ->prepend([
                'url' => url()->current(),
                'label' => 'First',
                'active' => false,
            ])
            ->push([
                'url' => url()->current(),
                'label' => 'Next',
                'active' => false,
            ])->push([
                'url' => url()->current(),
                'label' => 'Last',
                'active' => false,
            ]);

            $products['links'] = $links;
            $products['total'] = $n;
        }
        
        // dd($products);
        return $products;
    }
}
