<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductAttributes;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;

class ProductAttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = ProductAttributes::get();
        foreach($attributes as $attr){
            // dump($attr->getTranslations('title'));
            $titles_raw = $attr->getTranslations('title');
            $titles = [
                'en' => $titles_raw['en'],
                'trad' => $titles_raw['zh-HK'],
                'simp' => $titles_raw['zh-CN']
            ];
            $attr->titles = $titles;
        }

        // dd($attributes);
        return Inertia::render('Admin/Attributes', [
            'attributes' => $attributes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Forms/Attribute', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Attribute'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
        ])->validate();

        try {
            $newAttribute = new ProductAttributes;
            $newAttribute->name = slugify($request->titles['en'], new ProductAttributes, 'name');
            $newAttribute->title = $request->titles;
            $newAttribute->symbol = $request->symbol;
            $newAttribute->save();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'brand' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.attributes.index'))->with([
            'success' => __('The new attribute is saved!'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductAttributes  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(ProductAttributes $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductAttributes  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductAttributes $attribute)
    {
        if(!$attribute){
            return back()->withErros([
                'failed' => __('The attribute is not registered in the system.')
            ]);
        }

        $data = [
            'id' => $attribute->id,
            'titles' => $attribute->getTranslations('title'),
            'slug' => $attribute->name,
            'symbol' => $attribute->symbol,
        ];
        // dd($data);

        return Inertia::render('Admin/Forms/Attribute', [
            'mode' => 'update',
            'data' => $data,
            'title' => __('Edit a Attribute'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductAttributes  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductAttributes $attribute)
    {
        // dump($request);
        try {
            $attribute->title = $request->titles;
            $attribute->name = $request->slug;
            $attribute->symbol = $request->symbol;

            $attribute->save();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'attribute' => $th->getMessage(),
            ]);
        }
        // dd($attribute);

        return redirect(route('admin.attributes.index'))->with([
            'success' => __('The attribute #'.$attribute->id.' is updated!'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductAttributes  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductAttributes $attribute)
    {
        // dd($attribute);
        try {
            $attribute->delete();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The brand has been deleted.',
        ]);
    }
}
