<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\CategoryLocale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ProductCategoryController extends Controller
{
    protected $locales = ['en' => 'en', 'zh-HK' => 'hk', 'zh-CN' => 'cn'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        $cats = ProductCategory::with([
            'locale' => function ($query) use ($locale) {
                $query->where('locale', $locale);
            }
        ])
        ->orderBy('updated_at', 'desc')
        ->get();

        foreach ($cats as $cat) {
            $cat->parent = null !== $cat->parent_id ? getParentTitle($cat->parent_id) : 'Top';
            $cat->updated = Carbon::parse($cat->updated_at)->format("Y/m/d H:i:s");
        }
        // dd($cats);

        $categories = [
            'items' => $cats,
            'empty' => __('Not Found'),
        ];

        return Inertia::render('Admin/Categories', [
            'categories' => $categories,
        ]);
    }

    public function fetch(Request $request)
    {
        $cats = ProductCategory::with([
            'locale' => function ($query) {
                $query->where('locale', app()->getLocale());
            }
        ])
        ->orderBy('updated_at', 'desc')
        ->get();

        foreach ($cats as $cat) {
            $cat->parent = null !== $cat->parent_id ? getParentTitle($cat->parent_id) : 'Top';
            $cat->updated = Carbon::parse($cat->updated_at)->format("Y/m/d H:i:s");
        }
        // dd($cats);

        $categories = [
            'items' => $cats,
            'empty' => __('Not Found'),
        ];

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return abort(404);
        $categories = makeParentCategories();
        // dd($categories);
        return Inertia::render('Admin/Forms/Category', [
            'categories' => $categories,
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Category'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
            'titles.cn' => 'required|max:100',
            'titles.hk' => 'required|max:100',
        ])->validate();

        // dd($request);
        
        $category_locales = [];
        foreach($this->locales as $key => $lang){
            $category_locales[$key] = [
                'title' => $request->titles[$lang],
                'description' => $request->descriptions[$lang],
            ];
        }
        // dd($category_locales);
        $slug = slugify($request->titles['en'], new ProductCategory, 'type');

        try {
            $new = ProductCategory::create([
                'type' => $slug,
                'parent_id' => $request->parent_id,
                'featured' => $request->has('featured') ? $request->featured : 0,
                'commission' => $request->has('commission') ? $request->commission : 0.0,
            ]);

            if($request->has('code')){
                $new->code = $request->code;
            }else{
                $new->code = 1000 + $new->id;
            }
            
            $new->save();
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-desktop');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-tablet');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-mobile');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-desktop');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-tablet');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-mobile');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-desktop');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-tablet');
            $new->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-mobile');
        } catch (\Throwable $th) {
            return back()->withErrors([
                'category' => $th->getMessage(),
            ]);
        }

        try {
            foreach ($category_locales as $key => $loc) {
                CategoryLocale::insert([
                    'category_id' => $new->id,
                    'locale' => $key,
                    'title' => $loc['title'],
                    'description' => $loc['description'],
                ]);
            }
        } catch (\Throwable $th) {
            return back()->withErrors([
                'category-locale' => $th->getMessage(),
            ]);
        }
        
        return redirect(route('admin.categories.index'))->with([
            'success' => __('The new category is saved!'),
        ]);
    }

    /**
     * Display the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(ProductCategory $category)
    {
        return Inertia::render('Admin/Show/Categories', [
            'categories' => makeParentCategories(),
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Category'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $category)
    {
        // return abort(404);
        // dump($category);
        // dd($category->getMediaResource('banner'));
        $categories = makeParentCategories();
        // dd($categories);
        // dd($locales);
        $titles = [];
        $descriptions = [];
        $category_locales = $category->locales->groupBy('locale')->all();
        // dd($category_locales);
        foreach($this->locales as $lang => $loc){
            $cat = $category_locales[$lang][0];
            // dd($cat);
            $titles[$loc] = $cat->title;
            $descriptions[$loc] = $cat->description;
        }
        // dd($titles);
        $data = [
            'id' => $category->id,
            'parent_id' => $category->parent_id,
            'slug' => $category->type,
            'code' => $category->code,
            'featured' => $category->featured,
            'commission' => $category->commission,
            'titles' => $titles,
            'descriptions' => $descriptions,
            'banners' => [
                'en' => [
                    'desktop' => $category->getMediaResource('banner-en-desktop'),
                    'tablet' => $category->getMediaResource('banner-en-tablet'),
                    'mobile' => $category->getMediaResource('banner-en-mobile'),
                ],
                'hk' => [
                    'desktop' => $category->getMediaResource('banner-hk-desktop'),
                    'tablet' => $category->getMediaResource('banner-hk-tablet'),
                    'mobile' => $category->getMediaResource('banner-hk-mobile'),
                ],
                'cn' => [
                    'desktop' => $category->getMediaResource('banner-cn-desktop'),
                    'tablet' => $category->getMediaResource('banner-cn-tablet'),
                    'mobile' => $category->getMediaResource('banner-cn-mobile'),
                ],
            ],
        ];
        return Inertia::render('Admin/Forms/Category', [
            'categories' => $categories,
            'mode' => 'update',
            'data' => $data,
            'title' => __('Edit Category: '),
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $category)
    {
        // dd($request);
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
            'titles.cn' => 'required|max:100',
            'titles.hk' => 'required|max:100',
            'slug' => 'required|max:50',
        ])->validate();

        // dd($request);

        $category_locales = [];
        foreach ($this->locales as $key => $lang) {
            $category_locales[$key] = [
                'title' => $request->titles[$lang],
                'description' => $request->descriptions[$lang],
            ];
        }

        try {
            $category->type = $request->slug;
            $category->parent_id = $request->parent_id;
            $category->featured = $request->has('featured') ? $request->featured : 0;
            $category->commission = $request->has('commission') ? $request->commission : 0.0;
            if($request->has('code')){
                $category->code = $request->code;
            }
            
            $category->save();
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-desktop');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-tablet');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-en-mobile');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-desktop');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-tablet');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-hk-mobile');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-desktop');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-tablet');
            $category->addAllMediaFromTokens($request->input('tokens', []), 'banner-cn-mobile');

            foreach ($category_locales as $key => $loc) {
                DB::table('category_locales')->where('category_id', $category->id)->where('locale', $key)->update([
                    'title' => $loc['title'],
                    'description' => $loc['description'],
                ]);
            }
        } catch (\Throwable $th) {
            return back()->withErrors([
                'category' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.categories.index'))->with([
            'success' => __('The category #'.$category->id.' is updated!'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $category)
    {
        // return abort(404);
        // dd($category);
        try {
            ProductCategory::where('parent_id', $category->id)->delete();
            $category->delete();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The category has been deleted.',
        ]);
    }

    public function feature(Request $request)
    {
        // dd($request);
        $category = ProductCategory::find($request->id);
        // dd($category);
        $category->featured = !$category->featured;
        $category->save();

        // dd($category);
        return redirect()->route('admin.categories.index')->with([
            'success' => 'The category has been updated successfully!',
        ]);
    }
}
