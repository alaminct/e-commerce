<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductTags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ProductTagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = ProductTags::orderBy('created_at', 'desc')->get();
        
        foreach($tags as $tag){
            $tag->images = [
                'en' => $tag->getFirstMediaUrl('tag-img-en') ?: 'https://dummyimage.com/120x80/FCE7EC/000',
                'zh-HK' => $tag->getFirstMediaUrl('tag-img-hk') ?: 'https://dummyimage.com/120x80/FCE7EC/000',
                'zh-CN' => $tag->getFirstMediaUrl('tag-img-cn') ?: 'https://dummyimage.com/120x80/FCE7EC/000',
            ];
        }
        // dd($tags);
        return Inertia::render('Admin/Tags', [
            'tags' => $tags,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Forms/Tag', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Tag'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'labels.en' => 'required|max:100',
            'access' => 'required',
        ])->validate();
        // dd($request);
        try {
            $tag = new ProductTags;
            $tag->label = $request->labels;
            $tag->published = $request->published;
            $tag->access = $request->access;
            $tag->save();

            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-en');
            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-hk');
            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-cn');

        } catch (\Throwable $th) {
            return back()->withErrors([
                'tag' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.tags.index'))->with([
            'success' => __('The new tag is saved!'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductTags  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(ProductTags $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductTags  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductTags $tag)
    {
        if(!$tag){
            return back()->withErros([
                'failed' => __('The tag is not registered in the system.')
            ]);
        }

        $tag->labels = $tag->getTranslations('label');
        $tag->images = [
            'en' => $tag->getMediaResource('tag-img-en'),
            'hk' => $tag->getMediaResource('tag-img-hk'),
            'cn' => $tag->getMediaResource('tag-img-cn'),
        ];

        // dd($tag);
        return Inertia::render('Admin/Forms/Tag', [
            'mode' => 'update',
            'data' => $tag,
            'title' => __('Edit Tag #'.$tag->id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductTags  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductTags $tag)
    {
        Validator::make($request->all(), [
            'labels.en' => 'required|max:100',
            'access' => 'required|filled',
        ])->validate();
        
        try {
            $tag->label = $request->labels;
            $tag->published = $request->published;
            $tag->access = $request->access;
            $tag->save();

            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-en');
            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-hk');
            $tag->addAllMediaFromTokens($request->input('tokens', []), 'tag-img-cn');
        } catch (\Throwable $th) {
            return back()->withErrors([
                'tag' => $th->getMessage(),
            ]);
        }
        
        return redirect(route('admin.tags.index'))->with([
            'success' => __('The brand #'.$tag->id.' is updated!'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductTags  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductTags $tag)
    {
        $id = $tag->id;
        try {
            $tag->delete();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The tag #'.$id.' has been deleted!',
        ]);
    }
}
