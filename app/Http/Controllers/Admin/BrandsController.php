<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brands;
use App\Models\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brands::get();
        foreach($brands as $item){
            $item->logo = $item->getFirstMediaUrl('brand-logo') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
            $cats = null !== $item->categories ? getCategoryNamesByID(json_decode($item->categories, true)) : [];
            // dump($cats);
            $item->cats = $cats;
            $item->country = null !== $item->location ? getCountryName($item->location) : __('Not Set');
        }
        // dd($brands);
        return Inertia::render('Admin/Brands', [
            'brands' => $brands,
        ]);
    }

    public function fetch(Request $request)
    {
        $items = Brands::get();
        if($items->count() > 0){
            foreach($items as $item){
                $item->logo = $item->getFirstMediaUrl('brand-logo') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
            }
        }

        $brands = [
            'items' => $items,
            'empty' => __('Not Found'),
        ];

        return $brands;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Forms/Brand', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Brand'),
            'countries' => countryList(),
            'cats' => makeParentCategories(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
        ])->validate();

        // dd($request);
        try {
            $newBrand = new Brands;
            $newBrand->name = slugify($request->titles['en'], new Brands, 'name');
            $newBrand->title = $request->titles;
            $newBrand->description = $request->description;
            $newBrand->save();
            $newBrand->addAllMediaFromTokens($request->input('tokens', []), 'brand-logo');
        } catch (\Throwable $th) {
            return back()->withErrors([
                'brand' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.brands.index'))->with([
            'success' => __('The new brand is saved!'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brands $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brands $brand)
    {
        // dd($brand);
        if(!$brand){
            return back()->withErros([
                'failed' => __('The brand is not registered in the system.')
            ]);
        }

        $data = [
            'id' => $brand->id,
            'titles' => $brand->getTranslations('title'),
            'description' => $brand->description,
            'slug' => $brand->name,
            'logo' => $brand->getMediaResource('brand-logo'),
            'categories' => null !== $brand->categories ? json_decode($brand->categories) : null,
            'location' => $brand->location,
        ];
        // dd($data);

        return Inertia::render('Admin/Forms/Brand', [
            'mode' => 'update',
            'data' => $data,
            'title' => __('Edit a Brand'),
            'countries' => countryList(),
            'cats' => makeParentCategories(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brands $brand)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
        ])->validate();

        try {
            // dd($request);
            if($request->has('slug') && $request->slug !== $brand->name){
                $brand->name = slugify($request->slug, new Brands, 'name');
            }
            $brand->title = $request->titles;
            $brand->description = $request->description;
            $brand->location = $request->has('location') ? $request->location : null;
            $brand->categories = $request->has('categories') && count($request->categories) > 0 ? json_encode($request->categories) : null;
            $brand->save();

            $brand->addAllMediaFromTokens($request->input('tokens', []), 'brand-logo');
        } catch (\Throwable $th) {
            return back()->withErrors([
                'brand' => $th->getMessage(),
            ]);
        }

        return back()->with([
            'success' => __('The brand #'.$brand->id.' is updated!'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brands $brand)
    {
        // dd($brand);
        try {
            $brand->delete();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The brand has been deleted.',
        ]);
    }
}
