<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendor\Vendor;
use App\Models\User;
use Inertia\Inertia;
use App\Models\Brands;
use Carbon\Carbon;

use App\Models\Vendor\VendorInformation;
use App\Models\Vendor\VendorOtherInformation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Notifications\RejectVendorApplication;
class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendorusertemps = Vendor::get();

        foreach($vendorusertemps  as $item ){
           $item->br_file = $item->getFirstMediaUrl('br_file') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
           $item->componay_photo = $item->getFirstMediaUrl('componay_photo') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
         }
        return Inertia::render('Admin/Vendors/VendorApplications', [
            'vendorusertemps' => $vendorusertemps,
        ]);
    }

    public function fetch(Request $request)
    {
        $items = Vendor::get();
        foreach($vendorusertemps  as $item ){
            $item->br_file = $item->getFirstMediaUrl('br_file') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
            $item->componay_photo = $item->getFirstMediaUrl('componay_photo') ?: 'https://dummyimage.com/120x80/FCE7EC/000';
        }

        $vendorusertemps = [
            'items' => $items,
            'empty' => __('Not Found'),
        ];
        return $vendorusertemps;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Forms/Brand', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Brand'),
            'countries' => countryList(),
            // 'cats' => makeParentCategories(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
        ])->validate();


        try {
            $newBrand = new Brands;
            $newBrand->name = slugify($request->titles['en'], new Brands, 'name');
            $newBrand->title = $request->titles;
            $newBrand->description = $request->description;
            $newBrand->save();
            $newBrand->addAllMediaFromTokens($request->input('tokens', []), 'brand-logo');
        } catch (\Throwable $th) {
            return back()->withErrors([
                'brand' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.brands.index'))->with([
            'success' => __('The new brand is saved!'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VendorUserTem  $Vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $vendorUserTem=Vendor::with('vendorInformation','vendorOtherInformation')->where('id',$id)->first();
        if(!$id){
            return back()->withErros([
                'failed' => __('The Vendor User Application is not registered in the system.')
            ]);
        }
        $data = [

            'id'                   => $vendorUserTem->id ?? '',
            'company_name_en'      => $vendorUserTem->company_name_en ?? '',
            'company_name_ch'      => $vendorUserTem->company_name_ch ?? '',
            'incorporation_palce'  => $vendorUserTem->incorporation_palce ?? '',
            'incorporation_no'  => $vendorUserTem->incorporation_no ?? '',
             'business_name'       =>$vendorUserTem->business_name ?? '' ,
             'componay_website'    =>$vendorUserTem->componay_website ?? '',
             'floor'               =>$vendorUserTem->floor ?? '',
             'street'              =>$vendorUserTem->street ?? '',
             'district'            =>$vendorUserTem->district ?? '',
             'br_file'              =>$vendorUserTem->br_file ?? '',
             'componay_photo'       =>$vendorUserTem->componay_photo ?? '',
             'country'             =>$vendorUserTem->country ?? '',
             'business_registation_no' =>$vendorUserTem->business_registation_no ??'',
             'br_date'                =>$vendorUserTem->br_date ?? '',
             'online_shop_status'    =>$vendorUserTem->online_shop_status ?? '',
             'product_qty'           =>$vendorUserTem->product_qty ??'',
             'registrant_name'       =>$vendorUserTem->vendorInformation->registrant_name ?? '',
             'registrant_contact_number'=>$vendorUserTem->vendorInformation->registrant_contact_number ?? ""  ,
             'registrant_posotion'    =>$vendorUserTem->vendorInformation->registrant_posotion ?? '',
             'registrant_email'       =>$vendorUserTem->vendorInformation->registrant_email ?? '',
             'contact_name'           =>$vendorUserTem->vendorOtherInformation->contact_name ?? '',
             'contact_person_position' =>$vendorUserTem->vendorOtherInformation->contact_person_position ?? '',
             'contact_email'           =>$vendorUserTem->vendorOtherInformation->contact_email ??'',
             'mobileno_contact'        =>$vendorUserTem->vendorOtherInformation->mobileno_contact ?? '',
             'status'                  =>$vendorUserTem->vendorOtherInformation->status ?? "",
        ];

        return Inertia::render('Admin/Forms/VendorApplicant', [
            'mode' => 'Vendor Applicant ',
            'data' => $data,
            'title' => __('Vendor Aplicant profile

            '),
        ]);

    }



    public function update(Request $request, Vendor $Vendor)
    {
        Validator::make($request->all(), [
            'titles.en' => 'required|max:100',
        ])->validate();

        try {

            if($request->has('slug')){
                $brand->name = slugify($request->slug, new Brands, 'name');
            }
            $brand->title = $request->titles;
            $brand->description = $request->description;
            $brand->location = $request->has('location') ? $request->location : null;
            $brand->categories = $request->has('categories') && count($request->categories) > 0 ? json_encode($request->categories) : null;
            $brand->save();

            $brand->addAllMediaFromTokens($request->input('tokens', []), 'brand-logo');
        }
         catch (\Throwable $th) {
            return back()->withErrors([
                'brand' => $th->getMessage(),
            ]);
        }
        return redirect(route('admin.brands.index'))->with([
            'success' => __('The brand #'.$brand->id.' is updated!'),
        ]);
    }

     public function approved(Request $request){

            $vendor_id= $request->aplicantvendor;
            $vendor = Vendor::where('id', $vendor_id)->first();
            $vendororherinformation = VendorOtherInformation::where('vendor_id', $vendor_id)->first();

            $email_check = User::where('email',$vendororherinformation->registrant_email)->first();

            if( isset($email_check) && $email_check->mobile ==  $vendororherinformation->registrant_email){
                    return redirect(route('admin.aplicantvendors.index'))->with([
                        'success' => __('This Email Address Already Taken !'),
                    ]);
            }


            if( isset($email_check) && $email_check->mobile ==  $vendororherinformation->registrant_contact_number){
                return redirect(route('admin.aplicantvendors.index'))->with([
                    'success' => __('This Mobile Number  Already Taken !'),
                ]);
        }

            $newUser             =  New User;
            $newUser->name       =  $vendororherinformation->registrant_name;
            $newUser->email      = $vendororherinformation->registrant_email ;
            $newUser->current_team_id  = '0';
            $newUser->nickname  = $vendororherinformation->registrant_name.'vn'.$vendor_id ;
            $newUser->password  = Hash::make('vendor12345');
            $newUser->mobile    = $vendororherinformation->registrant_contact_number;
            $newUser->roles     = json_encode(["vendor" ]);
            $newUser->username  = $vendororherinformation->registrant_name.'vn'.$vendor_id;
            $newUser->save();

            $user_id= $newUser->id;

            $vendororherinformation->registrant_name;
            $vendor->user_id = $newUser->id;
            $vendor->status= '3';
            $vendor->save();

           if(isset($newUser)){
                return redirect(route('admin.aplicantvendors.index'))->with([
                    'success' => __('The new Vendor  Successfully Added !'),
                ]);
           }

     }

    public function rejected(Request $request){
        $vendor_id= $request->aplicantvendor;
        try {
            $vendororherinformation = VendorOtherInformation::where('vendor_id',  $vendor_id)->first();
            $vendororherinformation->notify(new RejectVendorApplication($vendororherinformation));
            $remove_vendor = Vendor::where('id',$vendor_id)->first();
            $remove_vendor->delete();
            $VendorInformation = VendorInformation::where('vendor_id', $request->aplicantvendor)->first();
            $VendorInformation->delete();
            $VendorOtherInformation = VendorOtherInformation::where('vendor_id', $request->aplicantvendor)->first();
            $VendorOtherInformation->delete();
        }
        catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }
        return back()->with([
             'success' => 'The Vendor Application  has been deleted.',
        ]);

    }

}
