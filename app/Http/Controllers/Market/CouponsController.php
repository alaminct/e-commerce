<?php

namespace App\Http\Controllers\Market;

use App\Http\Controllers\Controller;
use App\Models\Coupons;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use SecTheater\Marketplace\Facades\CouponRepository as Coupon;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupons::orderBy('created_at', 'desc')->get();
        // dd($coupons);
        return Inertia::render('Admin/Coupons', [
            'coupons' => $coupons,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Forms/Coupon', [
            'mode' => 'create',
            'data' => null,
            'title' => __('Create New Coupon'),
            'cats' => makeParentCategories(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        Validator::make($request->all(), [
            'target' => 'required|alpha',
            'code' => 'required|alpha_num',
            'amount' => 'required',
            'min_order' => 'required',
            'max_discount' => 'required',
            'is_percent' => 'required',
            'percentage' => 'required',
            'started_at' => 'required',
            'expires_at' => 'required',
        ])->validate();
        
        try {
            $coupon = Coupon::generate([
                'amount' => intval($request->amount),
                'code' => $request->code,
                'expires_at' => Carbon::parse($request->expires_at)->format('Y-m-d H:i:s'),
                'percentage' => $request->percentage,
            ]);
            $coupon->target = $request->target;
            $coupon->min_order = floatval($request->min_order);
            $coupon->max_discount = floatval($request->max_discount);
            $coupon->is_percent = floatval($request->is_percent);
            $coupon->started_at = Carbon::parse($request->started_at)->format('Y-m-d H:i:s');
            $coupon->categories = $request->categories;
            $coupon->products = $request->products;
            $coupon->save();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'coupon' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.coupons.index'))->with([
            'success' => __('The new coupon is saved!'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupons  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupons $coupons)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupons  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupons $coupon)
    {
        if(!$coupon){
            return back()->withErros([
                'failed' => __('The coupon is not registered in the system.')
            ]);
        }


        return Inertia::render('Admin/Forms/Coupon', [
            'mode' => 'update',
            'data' => $coupon,
            'title' => __('Edit Coupon "#'.$coupon->code.'"'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupons  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupons $coupon)
    {
        try {
            $coupon->amount = intval($request->amount);
            $coupon->code = $request->code;
            $coupon->target = $request->target;
            $coupon->expires_at = Carbon::parse($request->expires_at)->format('Y-m-d H:i:s');
            $coupon->percentage = $request->percentage;
            $coupon->min_order = floatval($request->min_order);
            $coupon->max_discount = floatval($request->max_discount);
            $coupon->is_percent = floatval($request->is_percent);
            $coupon->started_at = Carbon::parse($request->started_at)->format('Y-m-d H:i:s');
            $coupon->categories = $request->categories;
            $coupon->products = $request->products;
            $coupon->save();

            Coupon::regenerate($coupon->id, $request->code);
        } catch (\Throwable $th) {
            return back()->withErrors([
                'coupon' => $th->getMessage(),
            ]);
        }

        return redirect(route('admin.coupons.index'))->with([
            'success' => __('The coupon "'.$coupon->code.'" is updated!'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupons  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupons $coupon)
    {
        // dd($coupon);
        $code = $coupon->code;
        try {
            $coupon->delete();
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The coupon "'.$code.'" has been deleted.',
        ]);
    }

    /**
     * Deactivate a coupon
     *
     * @param Coupons $coupon
     * @return void
     */
    public function deactivate(Coupons $coupon)
    {
        // dd($coupon);
        $code = $coupon->code;
        try {
            Coupon::deactivate($coupon->id);
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The coupon "'.$code.'" has been deactivated.',
        ]);
    }

    public function reactivate(Coupons $coupon)
    {
        // dd($coupon);
        $code = $coupon->code;
        try {
            Coupon::activate($coupon->id);
        } catch (\Throwable $th) {
            return back()->withErrors([
                'failed' => $th->getMessage()
            ]);
        }

        return back()->with([
            'success' => 'The coupon "'.$code.'" has been re-activated.',
        ]);
    }
}
