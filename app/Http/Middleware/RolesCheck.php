<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RolesCheck
{
    protected $admin = ['dev-admin', 'admin'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if(!$user){
            return redirect('login');
        }

        // dd($user->roles);
        if(!in_array('dev-admin', $user->roles)){
            return redirect(route('dashboard.home', ['user' => $user->id]));
        }

        if (!in_array('admin', $user->roles)) {
            return redirect(route('dashboard.home', ['user' => $user->id]));
        }

        return $next($request);
    }
}
