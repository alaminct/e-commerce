<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MustUseDefaultLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(config('app.locale'));

        if(config('app.locale') !== 'zh-HK'){
            session(['locale' => 'zh-HK']);
            app()->setLocale('zh-HK');
        }

        return $next($request);
    }
}
