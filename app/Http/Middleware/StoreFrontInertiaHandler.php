<?php

namespace App\Http\Middleware;

use Cart;
use Closure;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use KgBot\LaravelLocalization\Facades\ExportLocalizations as ExportLocalization;

class StoreFrontInertiaHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Inertia::setRootView('store');
        $events = [
            [
                'title' => '2 0 2 0 冬 至 特 賣 區',
                'url' => '#',
                'bgcolor' => '#c12b57',
            ],
            [
                'title' => '年 終 精 選 禮 物 特 區',
                'url' => '#',
                'bgcolor' => '#707c9d',
            ],
        ];
        $locale = app()->getLocale();
        // dump($locale);
        $mainCategories = ProductCategory::whereNull('parent_id')->with([
            'locale' => function ($query) use ($locale) {
                $query->where('locale', $locale);
            }
        ])
        ->whereHas('locale')
        ->orderBy('id', 'asc')->get();

        // dd($mainCategories);
        if($mainCategories->count() < 1){
            return abort(403, 'Something is wrong!');
        }
        
        $cart = 0;
        // dump($request->user());
        if(Auth::check()){
            if(null !== Cart::item()){
                $cart = [
                    'items' => Cart::item(),
                    'count' => Cart::item()->count(),
                    'amount' => Cart::item()->count() > 0 ? Cart::item()->sum('total') : 0,
                ];
            }else{
                if (!$request->session()->has('cart')) {
                    $request->session()->put('cart', collect([]));
                }

                $cart = [
                    'items' => $request->session()->get('cart'),
                    'count' => $request->session()->get('cart')->count(),
                    'amount' => $request->session()->get('cart')->count() > 0 ? $request->session()->get('cart')->sum('total') : 0.0,
                ];
            }
        }else{
            // dd($request);
            if(!$request->session()->has('cart')){
                $request->session()->put('cart', collect([]));
            }
            $cart = [
                'items' => $request->session()->get('cart'),
                'count' => $request->session()->get('cart')->count(),
                'amount' => $request->session()->get('cart')->count() > 0 ? $request->session()->get('cart')->sum('total') : 0.0,
            ];
        }
        // dd($cart);

        // translation text sent through Inertia handler
        $messages = ExportLocalization::export()->toFlat();

        Inertia::share([
            'appUrl' => config('app.url'),
            'events' => $events,
            'mainCategories' => $mainCategories,
            'cart' => $cart,
            'notification' => 0,
            'demo' => [
                'home' => '/demo/home/',
            ],
            'messages' => $messages,
            'lang' => app()->getLocale(),
            'fallbackLocale' => config('app.fallback_locale'),
            'isAdmin' => hasRole('dev-admin')|| hasRole('admin'),
            'bonus' => [
                'freeShipping' => [
                    'amount' => 500.00,
                    'currency' => 'HK$'
                ],
            ],
            'flash' => [
                'status' => $request->session()->get('status'),
            ],
        ]);
        return $next($request);
    }
}
