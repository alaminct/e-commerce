<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use KgBot\LaravelLocalization\Facades\ExportLocalizations as ExportLocalization;

class AdminInertiaHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(Auth::check());
        // dump(session('locale'));
        if(!Auth::check()){
            return redirect()->route('admin.login');
        }

        if(!in_array('dev-admin', Auth::user()->roles)){
            if(!in_array('admin', Auth::user()->roles)){
                return redirect()->intended(route('dashboard.index', ['user' => Auth::user()->nickname]));
            }
        }
        Inertia::setRootView('admin');
        Inertia::share([
            'messages' => ExportLocalization::export()->toFlat(),
            'lang' => app()->getLocale(),
            'fallbackLocale' => config('app.fallback_locale'),
            'isDevAdmin' => hasRole('dev-admin'),
            'flash' => [
                'success' => $request->session()->get('success'),
            ],
        ]);
        return $next($request);
    }
}
