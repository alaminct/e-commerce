<?php

namespace App\Http\View\Composers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use KgBot\LaravelLocalization\Facades\ExportLocalizations as ExportLocalization;

class MainComposer
{
	/**
	 * Create a new profile composer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Dependencies automatically resolved by service container...
	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$messages = ExportLocalization::export()->toFlat();
		$view->with( compact( 'messages' ) );
	}

}