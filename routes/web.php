<?php
/* only in feature branch */

use App\Actions\Fortify\CreateNewUser;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BrandsController;
use App\Http\Controllers\Admin\ProductAttributesController;
use App\Http\Controllers\Admin\ProductCategoryController as AdminCategoriesController;
use App\Http\Controllers\Admin\ProductsController as AdminProductsController;
use App\Http\Controllers\Admin\ProductTagsController as AdminTagsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Front\StoreFrontController;
use App\Http\Controllers\Market\CouponsController;
use App\Http\Controllers\Market\ProductsController as MarketProductsController;
use App\Http\Controllers\Vendor\VendorController;
use App\Http\Controllers\Admin\VendorsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{locale}', [LocalizationController::class, 'switch'])->name('lang');
Route::get('gs-admin/login', [LoginController::class, 'admin'])->name('admin.login');
Route::get('login', function(){
    return redirect()->route('admin.login');
});

//////////////////////  Vendor Registation  alamin //////////////////////////////////////////

Route::resource('vendor-registation', VendorController::class)->except(['show']);

Route::post('gs-admin/login', [LoginController::class, 'adminpost'])->name('admin.login.enter');
Route::post('dashboard/logout', [DashboardController::class, 'logout'])->name('dashboard.logout');

Route::name('admin.')->prefix('gs-admin')->middleware(['web', 'inertia.admin'])->group(function () {
    Route::get('/', [AdminController::class, 'index'])->name('index');
    Route::post('logout', [AdminController::class, 'logout'])->name('logout');

    Route::resource('brands', BrandsController::class)->except(['show']);
    Route::put('/coupons/{coupon}/deactivate', [ CouponsController::class, 'deactivate'])->name('coupons.deactivate');
    Route::put('/coupons/{coupon}/reactivate', [ CouponsController::class, 'reactivate'])->name('coupons.reactivate');
    Route::resource('coupons', CouponsController::class)->except(['show']);
    Route::resource('products', AdminProductsController::class)->except(['show']);
    Route::resource('attributes', ProductAttributesController::class)->except(['show']);
    Route::put('categories/feature', [AdminCategoriesController::class, 'feature'])->name('categories.feature');
    Route::resource('categories', AdminCategoriesController::class)->except(['show']);
    Route::resource('tags', AdminTagsController::class)->except(['show']);
    // Route::resource('vendor-aplicant', VendorsController::class)->except(['show']);
    Route::resource('aplicantvendors', VendorsController::class);
    Route::get('aplicantvendors/fetch', [VendorsController::class, 'fetch'])->name('vendor-aplicant.fetch');

});

Route::middleware('inertia.store')->group(function(){
    Route::name('login.')->prefix('login')->group(function () {
        Route::get('/member', [LoginController::class, 'member'])->name('member');
        Route::get('/vendor', [LoginController::class, 'vendor'])->name('vendor');
        Route::get('/corporate', [LoginController::class, 'corporate'])->name('corporate');
        Route::get('/forgot-password', [PasswordController::class, 'forgotPassword'])->name('forgot-password');
        Route::post('/post/send-password', [PasswordController::class, 'sendPassword'])->middleware('throttle:5,1')->name('send-password');
        Route::post('/post/{type?}', [LoginController::class, 'login'])->middleware('throttle:5,1')->name('post');
    });

    // Route::get('password/reset/{token}/{email}', [PasswordController::class, 'resetPassword'])->middleware('guest')->name('password.form.reset');
    Route::post('password/post/reset', [PasswordController::class, 'resetPassword'])->middleware('guest')->name('password.post.update');
    Route::get('register/{type?}', [RegisterController::class, 'open'])->name('register.open');
    Route::post('register', [RegisterController::class, 'post'])->name('register.post');

    Route::name('products.')->prefix('product')->group(function () {
        // Route::get('test', [MarketProductsController::class, 'test'])->name('test');
    });

    Route::name('dashboard.')->prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
        Route::get('/{user:nickname}', [DashboardController::class, 'index'])->name('index');
        Route::get('settings', [DashboardController::class, 'settings'])->name('settings');
    });

    Route::name('storefront.')->group(function () {

        Route::get('/', [StoreFrontController::class, 'index'])->name('home');
        Route::get('/terms', [StoreFrontController::class, 'terms'])->name('terms');
        Route::get('/faq', [StoreFrontController::class, 'faq'])->name('faq');
        Route::get('/privacy', [StoreFrontController::class, 'privacy'])->name('privacy');
        Route::get('/video-tutorial', [StoreFrontController::class, 'video_tutorial'])->name('video-tutorial');
        Route::get('/talent-acquisition', [StoreFrontController::class, 'talent_acquisition'])->name('talent-acquisition');
        Route::get('/beginner-guide', [StoreFrontController::class, 'beginner_guide'])->name('beginner-guide');
        Route::get('/company-profile', [StoreFrontController::class, 'company_profile'])->name('company-profile');
        Route::get('/shipping-policy', [StoreFrontController::class, 'shipping_policy'])->name('shipping-policy');
        Route::get('category/{type?}', [StoreFrontController::class, 'category'])->name('category');


        #### Routes on this border will be deleted when the backend function is ready.
        #### Temporary routes for front-end design.
        Route::get('/product', function () {
            return Inertia\Inertia::render('Store/SingleProduct', [
                'meta' => [
                    'title' => 'Product'
                ],
                'page' => 'Product',
            ])
                ->withViewData(['title' => 'Product']);
        })->name('product');

        Route::get('/shopping-cart', function () {
            return Inertia\Inertia::render('Store/ShoppingCart', [
                'meta' => [
                    'title' => 'Shopping Cart'
                ],
                'page' => 'shopping-cart',
            ])
                ->withViewData(['title' => 'Shopping Cart']);
        })->name('shopping-cart');

        Route::get('/shopping-cart-summary', function () {
            return Inertia\Inertia::render('Store/ShoppingCartSummary', [
            'meta' => [
                'title' => 'Shopping Cart Summary'
            ],
            'page' => 'shopping-cart-summary',
            ])
            ->withViewData(['title' => 'Shopping Cart Summary']);
        })->name('shopping-cart-summary');

        Route::get('/vip-membership-plan', function () {
            return Inertia\Inertia::render('Auth/VipMembershipPlan', [
                'meta' => [
                    'title' => 'Vip Membership Plan'
                ],
                'page' => 'Vip-Membership-Plan',
            ])
                ->withViewData(['title' => 'Vip-Membership-Plan']);
        })->name('vip-membership-plan');

        Route::get('/promotion-news', function () {
            return Inertia\Inertia::render('Auth/PromotionNews', [
                'meta' => [
                    'title' => 'Promotion News'
                ],
                'page' => 'Promotion-News',
            ])
                ->withViewData(['title' => 'Promotion-News']);
        })->name('promotion-news');

        Route::get('/order-record', function () {
            return Inertia\Inertia::render('Auth/OrderRecord', [
                'meta' => [
                    'title' => 'Order Record'
                ],
                'page' => 'Order-Record',
            ])
                ->withViewData(['title' => 'Order-Record']);
        })->name('order-record');

        Route::get('/gps-activity-link-sharing', function () {
            return Inertia\Inertia::render('Auth/GPSActivityLinkSharing', [
                'meta' => [
                    'title' => 'GPS activity and link sharing Record'
                ],
                'page' => 'gps-activity-link-sharing',
            ])
                ->withViewData(['title' => 'gps-activity-link-sharing']);
        })->name('gps-activity-link-sharing');
        Route::get('/coupon-codes', function () {
            return Inertia\Inertia::render('Auth/CouponCodes', [
                'meta' => [
                    'title' => 'Coupon Codes'
                ],
                'page' => 'Coupon-Codes',
            ])
                ->withViewData(['title' => 'Coupon-Codes']);
        })->name('coupon-codes');

        Route::get('/account-details', function () {
            return Inertia\Inertia::render('Auth/AccountDetails', [
                'meta' => [
                    'title' => 'Account-Details'
                ],
                'page' => 'Account-Details',
            ])
                ->withViewData(['title' => 'Account-Details']);
        })->name('account-details');

        Route::get('/shipping-address', function () {
            return Inertia\Inertia::render('Auth/ShippingAddress', [
                'meta' => [
                    'title' => 'Shipping-Address'
                ],
                'page' => 'Shipping-Address',
            ])
                ->withViewData(['title' => 'Shipping-Address']);
        })->name('shipping-address');

        Route::get('/product-review', function () {
            return Inertia\Inertia::render('Auth/ProductReview', [
                'meta' => [
                    'title' => 'Product-Review'
                ],
                'page' => 'Product-Review',
            ])
                ->withViewData(['title' => 'Product-Review']);
        })->name('product-review');

        Route::get('/direct-customer-to-review-product', function () {
            return Inertia\Inertia::render('Auth/DirectCustomerToReviewProduct', [
                'meta' => [
                    'title' => 'Direct-Customer-To-Review-Product'
                ],
                'page' => 'Direct-Customer-To-Review-Product',
            ])
                ->withViewData(['title' => 'Direct-Customer-To-Review-Product']);
        })->name('direct-customer-to-review-product');

        Route::get('/payment-successful', function () {
            return Inertia\Inertia::render('Store/PaymentSuccessful', [
                'meta' => [
                    'title' => 'Payment-Successful'
                ],
                'page' => 'Payment-Successful',
            ])
                ->withViewData(['title' => 'Payment-Successful']);
        })->name('payment-successful');

        Route::get('/search-products', function () {
            return Inertia\Inertia::render('Auth/SearchProducts', [
                'meta' => [
                    'title' => 'Search-Products'
                ],
                'page' => 'Search-Products',
            ])
                ->withViewData(['title' => 'Search-Products']);
        })->name('search-products');


        ### Temporary routes for front-end design.
        ### Routes on this border will be deleted when the backend function is ready.

        Route::get('/{slug?}', [StoreFrontController::class, 'page'])->name('page');
    });
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return Inertia\Inertia::render('Dashboard');
// })->name('dashboard');


