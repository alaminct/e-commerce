const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
        './node_modules/hooper/dist/hooper.css',
    ],
    theme: {
        backgroundColor: theme => ({
            ...theme('colors'),
            'primary': '#c12b57',
            'secondary': '#707c9d',
            'danger': '#e3342f',
        }),
        backgroundSize: {
            'auto': 'auto',
            'cover': 'cover',
            'contain': 'contain',
            'fullsize': '100% 100%' 
        },
        extend: {
            inset: {
                '1/10': '10%',
                '18':'18%',
                '1/5': '20%',
                '1/6':'33%',
                '64': '64%',
                '78': '78%',
                '30': '30%',
                '60': '60%',
                '13': '13%',
                '44': '44%',
                '48': '48%',
                '6': '6',
                '20np': '-20px',
                '13np': '-13px',
                '75np': '75px',
                '5np': '5px',
                '9np': '9px'

            },
            fontSize: {
                '9px': '9px'
            },
            gridColumn: {
                'span-14': 'span 14 / span 14',
               },
            backgroundImage: {
                'top-bar': "url('/images/banners/new-top-header.jpg')",
                'mobile-header': "url('/images/banners/mobile.png')",
                'footer': "url('/images/banners/footer-background.jpg')",
                'productsidebar1': "url('/images/icons/productsidebar1.jpg')",
                'productsidebar2': "url('/images/icons/productsidebar2.svg')",
                'truck': "linear-gradient(to right, white 0%,white 76%,#ffeff2 76%,#ffeff2 100%,#ffeff2 100%)"
            },
            maxHeight: {
                '105': '36rem',
            },
            width: {
                '15': '15%',
                'inherit-width': 'initial',
                '89':'89%',
                '70':'70%'
            },
            minWidth: {
                '0': '0',
                '1/10': '10%',
                '1/5': '20%',
                '1/6': '16.6%',
                '23':'23%',
                '1/4': '25%',
                '2/6': '33%',
                '1/2': '50%',
                '3/5': '60%',
                '70': '70%',
                '3/4': '75%',
                '4/5': '80%',
                'full': '100%',
            },
            maxWidth: {
                '1/10': '10%',
                '1/5': '20%',
                '23':'23%',
                '1/4': '25%',
                '1/2': '50%',
                '70': '70',
                '3/4': '75%',
                '4/5': '80%'
            },
            spacing: {
                '26': '26%',
                '30': '30%',
                '32': '32%',
                '40': '40%',
                '60': '60%',
                '29': '29%',
                '32': '32%',
                '25': '25%',
                '93': '93%',
                '87': '87%',
                'x4l': '44rem',
                '12vh': '12vh',
                '25vh': '25vh',
                '65vh': '65vh',
                '80vh': '80vh'
            },
            gridTemplateColumns: {
                // Simple 16 column grid
               '14': 'repeat(14, minmax(0, 1fr))'
            },
            borderRadius: {
                '4xl': '2.8rem',
                '8xl': '5.8rem'
            },
            maxHeight: {
                '105': '36rem',
            },
            colors: {
                pink: {
                    darkest: '#f0bdc6',
                    dark: '#f7bfcc',
                    darken: '#bd8790',
                    DEFAULT: '#fce7ec',
                    bordering: '#efc1c1',
                    light: '#f6eeef',
                    lightest: '#fcf9f7',
                    soft: '#f5eeee',
                    softstrong: '#f0e4e4',
                    softerstrong: '#E1CDCE',
                    softer: 'f3e3de',
                    strong: '#d4acb2',
                    stronger: '#e692a0',
                    strongest: '#d8546a',
                    item: '#fffbfc',
                    itemstrong: '#ffeff2',
                    product: '#ffdae0',
                    maroon: '#ba0101',
                    baby: '#FFE0E5',
                    babystrong: '#FFCDD5',
                    babydark: '#D4B7B9',
                    babystrongest: '#E898A6',
                    mobnavpink: '#EFDCDF'
                },
                grey: {
                    search: '#f8f8f8ff',
                    thin: '#c2c2c2',
                    soft: '#f1f1f1',
                    softstrong: '#e2e2e2',
                    strong: '#8c8c8c',
                    product: '#766c67',
                    carousel: '#f8f8f8',
                    DEFAULT: '#b0b0b0',
                    line: '#e8d1d4',
                    lineg:'#D3CBCC',
                    dark:'#A8928D',
                    borderdark: '#9B9B9B',
                    light: '#E3E3E3'
                },
                blue: {
                    light: '#3072a7',
                    lighter: '#6fd0e0'
                },
                purple: {
                    lightest: '#e0e7f4',
                    light: '#a7a0c7',
                    DEFAULT: '#707c9d'
                },
                red: {
                    DEFAULT: '#ff0000'
                },
                yellow: {
                    dark: '#d2b8b9'
                },
                link: {
                    brown: '#736357',
                    pink: '#dd849d',
                }
            },
            fontFamily: {
                sans: ['Roboto'],
                yahei: ['Microsoft yahei']
            },
        },
    },
    variants: {
        inset: ['responsive', 'hover', 'focus'],
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
        extend: {
            opacity: ['disabled'],
            cursor: ['disabled'],
            display: ['hover', 'focus', 'group-hover'],
            transform: ['hover', 'focus'],
        },
        width: ["responsive", "hover", "focus" , 'group-hover'],
        position: ["responsive", "hover", "focus"],
        textAlign: ["responsive", "hover", "focus"],
        overflow: ['hover', 'focus' ,'group-hover'],
        backgroundColor: ['checked','focus'],
        borderColor: ['checked'],
        ringColor: ['hover', 'active','focus'],
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
        require('@tailwindcss/aspect-ratio'),
        require('tailwind-forms')(),
        require('tailwindcss-textshadow'),
    ],
}