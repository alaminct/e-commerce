<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserCustomInfoToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title', 11)->after('id')->nullable();
            $table->string('nickname', 100)->after('name')->nullable();
            $table->string('mobile', 20)->after('email')->nullable();
            $table->integer('referer', false)->after('mobile')->nullable();
            $table->tinyInteger('promotion')->after('referer')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('nickname');
            $table->dropColumn('mobile');
            $table->dropColumn('referer');
            $table->dropColumn('promotion');
        });
    }
}
