<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorUserTemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_user_tems', function (Blueprint $table) {
            $table->id();
            $table->string('company_name_en',100);
            $table->string('company_name_ch',100);
            $table->string('incorporation_palce',100)->nullable();
            $table->integer('incorporation_no')->nullable();
            $table->string('business_name',100)->nullable();
            $table->string('componay_website',150)->nullable();
            $table->string('floor',20);
            $table->string('street',100);
            $table->string('district',100);
            $table->string('country',100);
            $table->string('business_registation_no');
            $table->date('br_date')->nullable();
            $table->string('br_file',100)->nullable();
            $table->string('componay_photo', 100)->nullable();
            $table->string('mobileno_contact', 100)->nullable();
            $table->string('product_qty')->nullable();
            $table->tinyInteger('online_shop_status')->nullable();
            $table->string('registrant_name')->nullable();
            $table->string('registrant_posotion')->nullable();
            $table->string('registrant_contact_number')->nullable();
            $table->string('registrant_email')->nullable();
            $table->string('contact_name',100);
            $table->string('contact_person_position',100);
            $table->string('contact_email',100);
            $table->string('status',20)->default('1')->comment("1 =>pending,2=>reviewed,3=>approved,4=>declined");
            $table->tinyInteger('accept_term',0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_user_tems');
    }
}