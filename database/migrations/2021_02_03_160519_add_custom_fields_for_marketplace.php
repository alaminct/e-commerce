<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomFieldsForMarketplace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('sku', 100)->nullable()->after('name');
            $table->string('number', 17)->nullable()->after('sku');
            $table->string('categorycode', 11)->nullable()->after('number');
            $table->boolean('licensed')->default(0)->after('categorycode');
            $table->boolean('bulk')->default(0)->after('licensed');
            $table->json('tags')->nullable()->after('bulk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('sku');
            $table->dropColumn('number');
            $table->dropColumn('categorycode');
            $table->dropColumn('licensed');
            $table->dropColumn('bulk');
            $table->dropColumn('tags');
        });
    }
}
