<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('company_name_en',100);
            $table->string('company_name_ch',100);
            // $table->string('company_name_hk',100);
           // $table->bigInteger('user_id')->nullable();
            $table->string('incorporation_palce',100)->nullable();
            $table->string('incorporation_no')->nullable();
            $table->string('business_name',100)->nullable();
            $table->string('componay_website',150)->nullable();
            $table->string('floor',20);
            $table->string('street',100);
            $table->string('district',100);
            $table->string('country',100);
            $table->string('business_registation_no');
            $table->date('br_date')->nullable();
            $table->string('product_qty')->nullable();
            $table->tinyInteger('online_shop_status')->nullable();
            $table->string('status',20)->default('1')->comment("1 =>pending,2=>reviewed,3=>approved,4=>declined");
            $table->tinyInteger('accept_term',0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
