<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSkuAndDetailsToProductVariationTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_variation_types', function (Blueprint $table) {
            $table->json('details')->nullable()->after('name');
            $table->string('sku', 100)->nullable()->after('details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_variation_types', function (Blueprint $table) {
            $table->dropColumn('details');
            $table->dropColumn('sku');
        });
    }
}
