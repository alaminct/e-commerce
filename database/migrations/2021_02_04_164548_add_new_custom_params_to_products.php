<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewCustomParamsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->json('categories')->nullable()->after('categorycode');
            $table->string('location', 3)->nullable()->after('tags');
            $table->float('share', 8, 2)->default(0.0)->after('price');
            $table->string('share_type', 11)->default('fixed')->nullable()->after('share');
            $table->string('status', 11)->default('reviewed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('categories');
            $table->dropColumn('location');
            $table->dropColumn('share');
            $table->dropColumn('share_type');
            $table->dropColumn('status');
        });
    }
}
