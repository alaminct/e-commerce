<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdditionalParamsForCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->tinyInteger('is_percent', false)->nullable()->default(0)->after('percentage');
            $table->string('target', 100)->default('total')->after('code');
            $table->dateTime('started_at')->nullable()->after('active');
            $table->json('categories')->nullable()->after('target');
            $table->json('products')->nullable()->after('categories');
            $table->float('min_order', 8, 2)->default(0.0)->nullable()->after('amount');
            $table->float('max_discount', 8, 2)->default(0.0)->nullable()->after('min_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('is_percent');
            $table->dropColumn('target');
            $table->dropColumn('started_at');
            $table->dropColumn('categories');
            $table->dropColumn('products');
            $table->dropColumn('min_order');
            $table->dropColumn('max_discount');
        });
    }
}
