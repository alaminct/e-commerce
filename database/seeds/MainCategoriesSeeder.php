<?php

namespace Database\Seeders;

use App\Models\CategoryLocale;
use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class MainCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'en' => 'Healthcare',
                'zh-HK' => '護理保健',
                'zh-CN' => '护理保健',
            ],
            [
                'en' => 'Skincare and Cosmetic',
                'zh-HK' => '護膚化妝',
                'zh-CN' => '护肤化妆',
            ],
            [
                'en' => 'Supermarket',
                'zh-HK' => '超級市場',
                'zh-CN' => '超级市场',
            ],
            [
                'en' => 'Masks & Sanitizer',
                'zh-HK' => '口罩消毒',
                'zh-CN' => '口罩消毒',
            ],
            [
                'en' => 'Mother & Baby care',
                'zh-HK' => '母嬰育兒',
                'zh-CN' => '母婴育儿',
            ],
            [
                'en' => 'Pet care',
                'zh-HK' => '寵物用品',
                'zh-CN' => '宠物用品',
            ],
            [
                'en' => 'Electronic appliances',
                'zh-HK' => '電子電器',
                'zh-CN' => '电子电子学',
            ],
            [
                'en' => 'Home furniture',
                'zh-HK' => '家品傢俬',
                'zh-CN' => '家品家俬',
            ],
            [
                'en' => 'Sports & Travel',
                'zh-HK' => '運動旅行',
                'zh-CN' => '运动旅行',
            ],
            [
                'en' => 'Fashion Clothing',
                'zh-HK' => '時尚服飾',
                'zh-CN' => '时尚服饰',
            ],
            [
                'en' => 'Overseas order',
                'zh-HK' => '海外訂購',
                'zh-CN' => '海外订购',
            ],
        ];

        foreach($categories as $titles){
            $string = $titles['en'];
            $category = new ProductCategory();
            // dd($category);

            $type = slugify($string, $category, 'type');
            // var_dump($type);
            try {
                $category->type = $type;
                $category->save();
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }

            foreach($titles as $locale => $title){
                CategoryLocale::insert([
                    'category_id' => $category->id,
                    'locale' => $locale,
                    'title' => $title,
                ]);
            }
        }
    }
}
