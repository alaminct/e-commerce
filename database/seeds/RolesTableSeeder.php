<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SecTheater\Marketplace\Models\EloquentRole;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = EloquentRole::create([
            'name'        => 'Customer',
            'slug'        => 'customer',
            'permissions' => [
                'create-category'      => false,
                'view-category'        => false,
                'update-category'      => false,
                'delete-category'      => false,
                'upgrade-user'    => false,
                'downgrade-user'  => false,
                'review-product' => true,
                'create-product' => false,
                'edit-product' => false,
                'delete-product' => false

            ],
        ]);
        $admin = EloquentRole::create([
            'slug'        => 'admin',
            'name'        => 'Administrator',
            'permissions' => [
                'create-category'      => true,
                'update-category'      => true,
                'delete-category'      => true,
                'view-category'        => true,
                'upgrade-user'    => true,
                'downgrade-user'  => true,
                'review-product' => true,
                'create-product' => true,
                'edit-product' => true,
                'delete-product' => true
            ],
        ]);
        $vendor = EloquentRole::create([
            'slug'        => 'vendor',
            'name'        => 'Vendor',
            'permissions' => [
                'create-category'      => true,
                'update-category'      => true,
                'view-category'        => true,
                'delete-category'      => false,
                'upgrade-user'    => false,
                'downgrade-user'  => false,
                'review-product' => false,
                'create-product' => true,
                'edit-product' => true,
                'delete-product' => true

            ],
        ]);
        $corporate = EloquentRole::create([
            'slug'        => 'corporate',
            'name'        => 'Corporate',
            'permissions' => [
                'create-category'      => true,
                'update-category'      => true,
                'view-category'        => true,
                'delete-category'      => false,
                'upgrade-user'    => true,
                'downgrade-user'  => false,
                'review-product' => true,
                'create-product' => true,
                'edit-product' => true,
                'delete-product' => true

            ],
        ]);
        
    }
}
