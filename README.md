## About Repository

This repository is private repository for the development of the new girlssecrets.com new web based application. It uses Laravel 8 with Jetstream on Inertia (Vue.JS).

### Requirements
Local environment please see: [documentation](https://laravel.com/docs)

## Localhost Setup

Clone this repository to your localhost and setup a proper vhost for it.
Then please follow the steps below:

1. `composer install`
2. setup your database, correct the values on the .env file and local virtualhost.
3. `npm install`
4. `npm run dev` or `npm run watch` (depends on your development style, I do watch because I use VSCode)
5. change "users.json.example" to "users.json", then edit the value as you need. Make sure you are using an SMTP trap for the email transaction.
5. `php artisan migrate:fresh --seed`
6. `composer dump-autoload -o` (There will warning message, just leave it be. This is a warning from cygwean for translation tool).

## Repository flow

1. Never push updates to master and develop branch

	- master branch will be hold for production, you can pull this branch to get most stable version but never push on it.
	- develop branch will be mainly used by me, this branch is where I collect all updates and merge them for staging server.


2. Updates or new features will need to be named per item. For example; If you're working on an AJAX feature to for payment, the branch name will be "features/ajax-paypal-payment".
	
	- If the above is too vague, then please create one branch specific for you with the name "feature/(your name of your team member name).
	- Better to create new branch rather than rolling back a branch, if you're unsure of an update I recommend you to create a new branch. I won't be hesitate to remove a branch if I found it creating a fatal error.

3. Bug fixes updates should be named under "hotfixes", so if there is an update to hotfix an issue and need a branch (if needed, please mark it) you can create a new branch with the name "hotfixes/bug-on-filter-product"

4. Please always check "master" branch, and prepare to merge with "master" branch as that is the most stable version.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
