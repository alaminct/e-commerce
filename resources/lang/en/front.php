<?php

return array (
  'carousel' => 
  array (
    'sold' => 'Sold',
    'titles' => 
    array (
      'daily' => 'Daily Needs Products',
      'epidemic' => 'Anti-Epidemic Products',
      'featured' => 'Featured Products',
      'limited' => 'Limited Time Offers',
      'winter' => 'Winter Selections',
    ),
  ),
  'cart' => 'Shopping Cart',
  'category' => 'Category',
  'menu' => 
  array (
    'home' => 'Home',
    'login' => 'Login',
    'register' => 'Sign up',
    'wishlist' => 'My Favorites',
  ),
  'mobile' => 
  array (
    'menu' => 'Menu',
  ),
  'search' => 
  array (
    'label' => 'Search product',
    'placeholder' => 'Enter brand or product name to search the entire store',
  ),
  'ship' => 
  array (
    'close' => ' To Free Shipping',
    'open' => '',
  ),
);
