<?php

return array (
  'failed' => '這些憑據與我們的記錄不匹配',
  'password' => '提供的密碼不正確',
  'throttle' => '嘗試登錄的次數過多。請在：seconds秒後重試',
);
