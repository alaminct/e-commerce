<?php
return [
    'menu' => [
        'wishlist' => '
我的最愛',
        'register' => '登記成為會員',
        'login' => '會員登入',
        'home' => '主頁',
    ],
    'category' => '商品種類',
    'cart' => '購物車',
    'ship' => [
        'open' => '選購多',
        'close' => ', 即免運費',
    ],
    'search' => [
        'placeholder' => '請輸入品牌或產品名稱，搜尋全場商品商品種類',
        'label' => '搜尋商品',
    ],
    'carousel' => [
        'titles' => [
            'featured' => '精選產品推介',
            'limited' => '限時搶購優惠',
            'winter' => '冬日護膚化妝精選',
            'daily' => '生活至抵貨品推介',
            'epidemic' => '必選防疫產品',
        ],
        'sold' => '已售出',
    ],
    'mobile' => [
        'menu' => 'Menu',
    ],
];
?>