<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '您的密碼已重置！',
    'sent' => '我們已經通過電子郵件發送了您的密碼重置鏈接！',
    'throttled' => '請稍等，然後重試。',
    'token' => '此密碼重置令牌無效。',
    'user' => "我們找不到使用該電子郵件地址的用戶。",
    'notif' => [
        'subject' => '重置密碼通知',
        'greet' => '您好!',
        'lines' => [
            '您收到此電子郵件是因爲我們收到了您帳戶的密碼重設請求。',
            '這個重置密碼鏈接將於 60 分鐘後過期。',
            '如果您未申請重置密碼，請忽略此郵件。',
        ],
        'action' => '重置密碼',
    ],
];
