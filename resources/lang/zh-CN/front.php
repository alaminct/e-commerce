<?php
return [
    'menu' => [
        'wishlist' => '我的最爱',
        'register' => '登记成为会员',
        'login' => '会员登入',
        'home' => '主页',
    ],
    'category' => '产品类别',
    'cart' => '购物车',
    'ship' => [
        'open' => '选购多',
        'close' => ', 即免运费',
    ],
    'search' => [
        'placeholder' => '请输入品牌或产品名称，搜寻全场商品商品种类',
        'label' => '搜寻商品',
    ],
    'carousel' => [
        'titles' => [
            'featured' => '精选产品推介',
            'limited' => '限时抢购优惠',
            'winter' => '冬日护肤化妆精选',
            'daily' => '生活至抵货品推介',
            'epidemic' => '必选防疫产品',
        ],
        'sold' => '已售出',
    ],
    'mobile' => [
        'menu' => 'Menu',
    ],
];
?>