<?php

return array (
  'failed' => '这些凭据与我们的记录不匹配',
  'password' => '提供的密码不正确',
  'throttle' => '尝试登录的次数过多。请在：seconds秒后重试',
);
