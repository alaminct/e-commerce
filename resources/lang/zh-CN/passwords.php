<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '您的密码已重置！',
    'sent' => '我们已经通过电子邮件发送了您的密码重置链接！',
    'throttled' => '请稍等，然后重试。',
    'token' => '此密码重置令牌无效。',
    'user' => "我们找不到使用该电子邮件地址的用户。",
    'notif' => [
        'subject' => '重置密码通知',
        'greet' => '您好!',
        'lines' => [
            '您收到此电子邮件是因为我们收到了您帐户的密码重设请求。',
            '这个重置密码链接将于 60 分钟后过期。',
            '如果您未申请重置密码，请忽略此邮件。',
        ],
        'action' => '重置密码',
    ],
];
